public with sharing class VacancyRatesCalculatorNewExtension {
	
	public Date currentDate{get;set;}
    
    public Boolean appliedFlatRate{get;set;}
    
    public VacancyRatesService.StateRateVariables stateRateVariables{get;set;}
   
    public String step{get;set;}
    
    public String selectedState{get;set;}
    public Date effectiveDate{get;set;}

    public VacancyRatesService.AllowanceVariable appliedAllowanceOption{get;set;}
    public List<VacancyRatesService.AllowanceVariable> allowanceVariables{get;set;}

    public String saveNormalRatesMessage{get;set;}
    public String saveAllowanceRatesMessage{get;set;}
    public Boolean allowanceRatesCalculated{get;set;}

    public VacancyRatesService.StateRateVariables normalRatesResult{get;set;}

    private Decimal super_per = 0;
    private Decimal payrollTax_per = 0;

    private VacancyRatesService service ;
    
    public static final String ALLOW_MEAL = 'Meal';
    public static final String ALLOW_CAR = 'Car';
    public static final String ALLOW_MOBILE = 'Mobile';
    public static final String ALLOW_TRAVEL = 'Travel';
    public static final String ALLOW_LAUNDRY = 'Laundry';
    
    ApexPages.StandardController stdController ;
    
    /**
     * Constructor
     */
    public VacancyRatesCalculatorNewExtension(ApexPages.StandardController stdController) {
        
        this.stdController = stdController;
        
        /**
         * initialise variables
         */
         appliedFlatRate = false;
        //indicate what step is operation - initial, calculated, pushed
        step = '';
        //indicate if allownance rates are calculated
        allowanceRatesCalculated = false;
        //feedback message after pushing to vacancy
        saveNormalRatesMessage = '';
        saveAllowanceRatesMessage = '';
        
        
 
        service = new VacancyRatesService(this);
        //get the start date of vacancy contract - default value is 
        effectiveDate = service.getContractStartDate(stdController.getId());
        //default state
        selectedState = 'NSW';
        //get the percentages of super and payroll tax according to state and effective date
        service.setStateAndEffectiveDate(selectedState, effectiveDate);
        Decimal newSuper = service.getSuperPer();
        Decimal newPayrollTax  = service.getPayrollTaxPer();
    
        /**
         * build rate variables of table 1 for user edit
         */
        List<VacancyRatesService.RateVariable> defaultRateVariables = new  List<VacancyRatesService.RateVariable>();
        defaultRateVariables.add(new VacancyRatesService.RateVariable (1, VacancyRatesService.BASE_RATE, true));
        defaultRateVariables.add(new VacancyRatesService.RateVariable (2, VacancyRatesService.CASUAL_LOADING));
        defaultRateVariables.add(new VacancyRatesService.RateVariable (3, VacancyRatesService.SHIFT_LOADING ));
        //pre-populate super percentage
        VacancyRatesService.RateVariable defaultSuper = new VacancyRatesService.RateVariable (4, VacancyRatesService.SUPERA );
         defaultRateVariables.add(
             defaultSuper.withColumnValue(VacancyRatesService.STANDARD_TIME_COL, newSuper)
            .withColumnValue(VacancyRatesService.SHITF_RATE_ONE_COL, newSuper)
            .withColumnValue(VacancyRatesService.SHITF_RATE_TWO_COL, newSuper));
        
        defaultRateVariables.add(new VacancyRatesService.RateVariable (5, VacancyRatesService.LSL_P));
        
        //pre-populate payroll tax percentage
        VacancyRatesService.RateVariable defaultPayrollTax = new VacancyRatesService.RateVariable (6, VacancyRatesService.PAYROLL_TAX);
         defaultRateVariables.add(
             defaultPayrollTax.withColumnValue(VacancyRatesService.STANDARD_TIME_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.SHITF_RATE_ONE_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.SHITF_RATE_TWO_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.TIME_AND_AHALF_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.DOUBLE_TIME_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.DOUBLE_TIME_AND_AHALF, newPayrollTax));
       
        defaultRateVariables.add( new VacancyRatesService.RateVariable (7, VacancyRatesService.WORK_COMP));
        defaultRateVariables.add( new VacancyRatesService.RateVariable (8, VacancyRatesService.MARK_UP));
        stateRateVariables = new VacancyRatesService.StateRateVariables(selectedState, defaultRateVariables);
       
        /**
         * build allowance variables in table 3 for user edit
         */
        allowanceVariables =  new List<VacancyRatesService.AllowanceVariable>();
        appliedAllowanceOption  = new  VacancyRatesService.AllowanceVariable (0, ' ');
        allowanceVariables.add(appliedAllowanceOption);
        allowanceVariables.add(new VacancyRatesService.AllowanceVariable (1, ''));
        allowanceVariables.add(new VacancyRatesService.AllowanceVariable (2, ''));
        allowanceVariables.add(new VacancyRatesService.AllowanceVariable (3, ''));
        allowanceVariables.add(new VacancyRatesService.AllowanceVariable (4, ''));
        allowanceVariables.add(new VacancyRatesService.AllowanceVariable (5, ''));
        
       
    }
    
    
    public pageReference knockDoor(){
    	 this.currentDate = Date.today();
    	 return null;
    }
 
    
    /**
     * Get available states from rates custom settings
     * 
     */
    public List<SelectOption> getStates(){
        List<SelectOption> options = new List<SelectOption>();
        if(service != null) {
           Set<String> states = service.getStates();
           if(states != null){
                //build select options for front end 
                for(String str : states){
                    options.add(new selectOption(str, str));
                }
               // options.sort();
           }
       }
       return options;
    }
    
    /**
     * Switch to/from flat rates
     */
    public pageReference switchFlatRate(){
        step = 'flatRateChanged';
        saveNormalRatesMessage = '';
        saveAllowanceRatesMessage = '';
        allowanceRatesCalculated = false;
        
        return null;
    }
    
    /**
     * Change state, get corresponding percentages of super and payroll tax and refresh them in tqble 1
     * 
     */
    public pageReference changeState(){
        stateRateVariables.state = this.selectedState;
        this.effectiveDate = service.getContractStartDate(stdController.getId());
        //get the percentages of super and payroll tax according to selected state and effective date
        service.setStateAndEffectiveDate(this.selectedState, this.effectiveDate);
        Decimal newSuper = service.getSuperPer();
        Decimal newPayrollTax  = service.getPayrollTaxPer();
        
        //refresh super percentage in table 1
        VacancyRatesService.RateVariable defaultSuper = stateRateVariables.getSuper();
        defaultSuper.withColumnValue(VacancyRatesService.STANDARD_TIME_COL, newSuper)
            .withColumnValue(VacancyRatesService.SHITF_RATE_ONE_COL, newSuper)
            .withColumnValue(VacancyRatesService.SHITF_RATE_TWO_COL,newSuper);
        //refresh payroll tax in table 1
        VacancyRatesService.RateVariable defaultPayrollTax = stateRateVariables.getPayrollTax();
        defaultPayrollTax.withColumnValue(VacancyRatesService.STANDARD_TIME_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.SHITF_RATE_ONE_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.SHITF_RATE_TWO_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.TIME_AND_AHALF_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.DOUBLE_TIME_COL, newPayrollTax)
            .withColumnValue(VacancyRatesService.DOUBLE_TIME_AND_AHALF, newPayrollTax);
            
        step='stateChanged';
        saveNormalRatesMessage = '';
        saveAllowanceRatesMessage = '';
        allowanceRatesCalculated = false;
        
        return null;
        
    }
    
    /**
     * Calculate the table 1 of rates
     * 
     */
    public pageReference calculateNormalRates(){
    
        saveNormalRatesMessage = '';
        saveAllowanceRatesMessage = '';
        allowanceRatesCalculated = false;
        
        //get the percentages of super and payroll tax according to selected state and effective date
        stateRateVariables.state = this.selectedState;
        this.effectiveDate = service.getContractStartDate(stdController.getId());
        service.setStateAndEffectiveDate(this.selectedState, this.effectiveDate);
        //assign stateRateVariables with user input variables in table 1 to service object to calculate 
        service.setStateRateVariables(stateRateVariables);
        
        /**
         * get applied percentages (super, lsl, work comp and payroll tax) for allowance rate calculation
         */
         appliedAllowanceOption.withAppliedOptions(true, 
                                                         Decimal.valueOf(stateRateVariables.getSuper().StandardTime),
                                                         Decimal.valueOf(stateRateVariables.getLsl().StandardTime),
                                                         Decimal.valueOf(stateRateVariables.getWorkComp().StandardTime),
                                                         Decimal.valueOf(stateRateVariables.getPayrollTax().StandardTime));

        /**
         * build result object to be shown in table 2
         */
        List<VacancyRatesService.RateVariable> resultRateVariables = new  List<VacancyRatesService.RateVariable>();
        //build the table datas with specific rows and columns
        for(Integer k = 1; k <= VacancyRatesService.RESULT_ROWS.size() ; k ++){
             //calculate each row
             VacancyRatesService.RateVariable rateVariable = calculateRow(k, VacancyRatesService.RESULT_ROWS[k - 1 ] , service);
             if( rateVariable != null){
                 //place calculated row into the result object
                 resultRateVariables.add( rateVariable);
             }
        }
         
        normalRatesResult = new VacancyRatesService.StateRateVariables(selectedState, resultRateVariables);
    
        
   
        step = 'normalRatesCalculated';
        return null;
    }
    
    /**
     * Calculate the table 3 of rates
     * 
     */
    public pageReference calculateAllowanceRates(){
        
        saveNormalRatesMessage = '';
        saveAllowanceRatesMessage = '';
        allowanceRatesCalculated = false;
        
        //stateRateVariables.state = this.selectedState;
        //service.setStateAndEffectiveDate(selectedState, this.effectiveDate);
        //service.setStateRateVariables(stateRateVariables);
        
        //Calculate rates for each row in table 3
        for(Integer i = 1; i < allowanceVariables.size(); i ++){
            
            VacancyRatesService.AllowanceVariable var = allowanceVariables.get(i);
            
            if(var.name == null || var.name.trim().equals('')){
                continue;
            }
            
            
            //sum the checked rates options
            Decimal factorPercents = 100.00;
            //super checked
            if(var.checkedSuper){
                factorPercents += this.appliedAllowanceOption.appliedSuper;
            }
            //Payroll Tax checked
            if(var.checkedPayrollTax){
                factorPercents += this.appliedAllowanceOption.appliedPayrollTax;
            }
            //LSL checked
            if(var.checkedLsl){
                factorPercents += this.appliedAllowanceOption.appliedLsl;
            }
            //Work Comp checked
            if(var.checkedWorkComp){
                factorPercents += this.appliedAllowanceOption.appliedWorkComp;
            }
            
            //calculate total cost rate
            var.totalCostRate = (Decimal.valueOf(var.costRate) * ( factorPercents / 100 )).setScale(2, RoundingMode.HALF_UP) ;
            //calculate charge rate
            var.chargeRate = (var.totalCostRate * (1.0 + Decimal.valueOf(var.adminFee) / 100) ).setScale(2, RoundingMode.HALF_UP) ;
            
        }
        
        
        //assign values to the variables which will be pushed to vacancy object
        String type1 =  allowanceVariables.get(1).name;
        String type2 = allowanceVariables.get(2).name;
        String type3 =  allowanceVariables.get(3).name;
        String type4 = allowanceVariables.get(4).name;
        String type5 = allowanceVariables.get(5).name;
        if((null == type1 || ''.equals(type1))
            && (null == type2 || ''.equals(type2))
            && (null == type3 || ''.equals(type3))
            && (null == type4 || ''.equals(type4))
            && (null == type5 || ''.equals(type5))){
                
            step = 'normalRatesCalculated';
        }else{
            step = 'allowanceRatesCalculated';
            allowanceRatesCalculated = true;
        }
      
        
        
        return null;
    }
    
    /**
     * push normal rates to vacancy object
     * 
     */
    public pageReference pushNormalRates(){
        
        Boolean saved = false;
        
        if( appliedFlatRate ){
            //push result calculated by flat rates to vacancy
            saved = service.saveFlatRates(stdController.getId(), normalRatesResult);
        }else{
            //push result calculated by normal rates to vacancy
            saved = service.saveNormalRates(stdController.getId(), normalRatesResult);
        }
        
       if( saved ){
           
            saveNormalRatesMessage = 'SUCCESS to push rates to vacancy ! ';
       }else{
           
            saveNormalRatesMessage = 'ERROR: Failed to push rates to vacancy !!!';
       }
       
       return null;
    }
    
    /**
     * push allowance rates to vacancy object
     * 
     */
    public pageReference pushAllowanceRates(){
        
        if(service.saveAllowanceRates(stdController.getId(), allowanceVariables)){
            saveAllowanceRatesMessage = 'SUCCESS to push Allowance Rates to vacancy ! ';
 
        }else{
            saveAllowanceRatesMessage = 'ERROR: Failed to push Allowance Rates to vacancy !!!';

        }
        
        return null;
    }
    
    /**
     * Calculate one row of table 2 according to the variables in table 1
     * 
     */
    private VacancyRatesService.RateVariable calculateRow(Integer index, String rowName, VacancyRatesService service){
        
        //build a formula object for specified row 
        VacancyRatesService.Formula formula = service.buildFormula(rowName);
        if( formula == null ){
            return null;
        }
        
        //create a RateVariable object for current row to be calculated by using previous Formula object - formula
        VacancyRatesService.RateVariable rateVariable = new VacancyRatesService.RateVariable( index, rowName);
        
        //Calculate the value of each column in current row
        for(Integer i = 0 ; i < VacancyRatesService.COLUMNS.size() ; i ++){
            
            rateVariable.withColumnValue(VacancyRatesService.COLUMNS[i], formula.calc(VacancyRatesService.COLUMNS[i]));
            
        }
         
        return rateVariable;
    }
    
}