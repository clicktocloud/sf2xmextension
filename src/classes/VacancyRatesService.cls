public class VacancyRatesService{
    
    
    public static final String BASE_RATE = 'Base Rate ($)';
    public static final String BASE_RATE_RATE = 'Base Rate';
    public static final String CASUAL_LOADING = 'Casual Loading (%)';
    public static final String CASUAL_LOADING_RATE = 'Casual Loading';
    public static final String SHIFT_LOADING = 'Shift Loading (%)';
    public static final String SHIFT_LOADING_RATE = 'Shift Loading';
    public static final String LSL_P = 'Long Service Leave (%)';
    public static final String LSL_RATE = 'Long Service Leave';
    public static final String WORK_COMP = 'Workers Compensation (%)';
    public static final String WORK_COMP_RATE = 'Workers Compensation';
    public static final String MARK_UP = 'Mark Up (%)';
    public static final String MARK_UP_RATE = 'Mark Up';
    public static final String ACTUAL_RATE = 'Actual Rate';
    public static final String SUPERA = 'Superannuation (%)';
    public static final String SUPER_RATE = 'Superannuation';
    public static final String PAYROLL_TAX = 'Payroll Tax (%)';
    public static final String PAYROLL_TAX_RATE = 'Payroll Tax';
    public static final String CHARGE_RATE = 'Charge Rate';

    //the titles to be shown in first column of table 2
    public static final String[] RESULT_ROWS = new String[]{
            BASE_RATE_RATE,CASUAL_LOADING_RATE,SHIFT_LOADING_RATE,
            ACTUAL_RATE, SUPER_RATE, LSL_RATE,PAYROLL_TAX_RATE,WORK_COMP_RATE, MARK_UP_RATE, CHARGE_RATE
            };
            

    public static final String STANDARD_TIME_COL = 'StandardTime';
    public static final String SHITF_RATE_ONE_COL = 'ShiftRateOne';
    public static final String SHITF_RATE_TWO_COL = 'ShiftRateTwo';
    public static final String TIME_AND_AHALF_COL = 'TimeAndAHalf';
    public static final String DOUBLE_TIME_COL = 'DoubleTime';
    public static final String DOUBLE_TIME_AND_AHALF = 'DoubleTimeAndAHalf';
    public static final String[] COLUMNS = new String[]{ STANDARD_TIME_COL, SHITF_RATE_ONE_COL, SHITF_RATE_TWO_COL, TIME_AND_AHALF_COL,DOUBLE_TIME_COL, DOUBLE_TIME_AND_AHALF};

    /**
     * decide if the super is applied to specified column
     */
    public static boolean isSuperApplied(String columnName){
       // return ! (TIME_AND_AHALF_COL.equals(columnName) ||  DOUBLE_TIME_COL.equals(columnName) || DOUBLE_TIME_AND_AHALF.equals(columnName));
       return true;
    }
    
    /**
     * decide if one row is highlight
     */
    public static boolean isHighlightRow(String rateType){
        return  BASE_RATE_RATE.equals(rateType) ||  ACTUAL_RATE.equals(rateType) || CHARGE_RATE.equals(rateType);
    }

    private VacancyRatesCalculatorNewExtension calculator;
    private Decimal superPer;
    private Decimal payrollTaxPer;
    private StateRateVariables stateRateVariables;
    
    /**
     * Constructor
     */
    public VacancyRatesService(VacancyRatesCalculatorNewExtension calculator,String state, Date effectiveDate, StateRateVariables stateRateVariables){
        this.calculator = calculator;
        
        this.superPer = getSuperByState(state, effectiveDate);
        this.payrollTaxPer = getPayrollTaxByState(state, effectiveDate);
        this.stateRateVariables = stateRateVariables;
    }
    
     public VacancyRatesService(VacancyRatesCalculatorNewExtension calculator){
        this.calculator = calculator;
    }
   
    public void setStateAndEffectiveDate( String state, Date effectiveDate ){
        this.superPer = getSuperByState(state, effectiveDate);
        this.payrollTaxPer = getPayrollTaxByState(state, effectiveDate);
    }
    
    public void setStateRateVariables(StateRateVariables stateRateVariables){
        this.stateRateVariables = stateRateVariables;
    }
    
    public Decimal getSuperPer(){
        return this.superPer;
    }
    
    public Decimal getPayrollTaxPer(){
        return this.payrollTaxPer;
    }
    
    public Boolean saveFlatRates(Id vacancyId, StateRateVariables resultRateVariables){
        
        
        /**
         * assign values to the variables which will be pushed to corresponding fields of vacancy object
         */
        //assign actual rates
        RateVariable actualRate = resultRateVariables.rateVariables.get(3);
        Decimal standardTimeActualRate = actualRate.valueOf(STANDARD_TIME_COL);
        Decimal overtimeActualRate = actualRate.valueOf( SHITF_RATE_ONE_COL);
       
        //assign charge rates
        RateVariable chargeRate = resultRateVariables.rateVariables.get(9);
        Decimal standardTimeChargeRate = chargeRate.valueOf( STANDARD_TIME_COL);
        Decimal overtimeChargeRate = chargeRate.valueOf( SHITF_RATE_ONE_COL);
       
        
        PeopleCloud1__Placement__c vacancy = new PeopleCloud1__Placement__c(
           id = vacancyId,
           PeopleCloud1__Candidate_Charge_Rate__c = standardTimeActualRate,
           Candidate_Pay_Rate_Flat_Overtime_2XM__c = overtimeActualRate,
          
           PeopleCloud1__Client_Charge_Rate__c = standardTimeChargeRate,
           Client_Charge_Rate_Flat_Overtime_2XM__c  = overtimeChargeRate,
           
           
           Candidate_Pay_Rate_Day_Shift__c = 0,
           Candidate_Pay_Rate_Night_Shift__c = 0,
           Candidate_Pay_Rate_1_5X__c = 0,
           Candidate_Pay_Rate_2X__c = 0,
           Candidate_Pay_Rate_2_5X__c = 0,
           
           
           Client_Charge_Rate_Day_Shift__c = 0,
           Client_Charge_Rate_Night_Shift__c = 0,
           Client_Charge_Rate_1_5X__c = 0,
           Client_Charge_Rate_2X__c = 0,
           Client_Charge_Rate_2_5X__c = 0
         
           );
           
        try{
            update vacancy;
        }catch(Exception e){
            System.debug(e);
            return false;
        }
           
        return true;
        
    }
    
    public Boolean saveNormalRates(Id vacancyId, StateRateVariables resultRateVariables){
        
        
        /**
         * assign values to the variables which will be pushed to corresponding fields of vacancy object
         */
        //assign actual rates
        RateVariable actualRate = resultRateVariables.rateVariables.get(3);
        Decimal standardTimeActualRate = actualRate.valueOf(STANDARD_TIME_COL);
        Decimal shiftRateOneActualRate = actualRate.valueOf( SHITF_RATE_ONE_COL);
        Decimal shiftRateTwoActualRate = actualRate.valueOf( SHITF_RATE_TWO_COL);
        Decimal timeAndAHalfActualRate = actualRate.valueOf( TIME_AND_AHALF_COL);
        Decimal doubleTimeActualRate = actualRate.valueOf( DOUBLE_TIME_COL);
        Decimal doubleTimeAndAHalfActualRate = actualRate.valueOf( DOUBLE_TIME_AND_AHALF);
        //assign charge rates
        RateVariable chargeRate = resultRateVariables.rateVariables.get(9);
        Decimal standardTimeChargeRate = chargeRate.valueOf( STANDARD_TIME_COL);
        Decimal shiftRateOneChargeRate = chargeRate.valueOf( SHITF_RATE_ONE_COL);
        Decimal shiftRateTwoChargeRate = chargeRate.valueOf( SHITF_RATE_TWO_COL);
        Decimal timeAndAHalfChargeRate = chargeRate.valueOf( TIME_AND_AHALF_COL);
        Decimal doubleTimeChargeRate = chargeRate.valueOf( DOUBLE_TIME_COL);
        Decimal doubleTimeAndAHalfChargeRate = chargeRate.valueOf( DOUBLE_TIME_AND_AHALF);
        
        PeopleCloud1__Placement__c vacancy = new PeopleCloud1__Placement__c(
           id = vacancyId,
           PeopleCloud1__Candidate_Charge_Rate__c = standardTimeActualRate,
           Candidate_Pay_Rate_Day_Shift__c = shiftRateOneActualRate,
           Candidate_Pay_Rate_Night_Shift__c = shiftRateTwoActualRate,
           Candidate_Pay_Rate_1_5X__c = timeAndAHalfActualRate,
           Candidate_Pay_Rate_2X__c = doubleTimeActualRate,
           Candidate_Pay_Rate_2_5X__c = doubleTimeAndAHalfActualRate,
           
           PeopleCloud1__Client_Charge_Rate__c = standardTimeChargeRate,
           Client_Charge_Rate_Day_Shift__c = shiftRateOneChargeRate,
           Client_Charge_Rate_Night_Shift__c = shiftRateTwoChargeRate,
           Client_Charge_Rate_1_5X__c = timeAndAHalfChargeRate,
           Client_Charge_Rate_2X__c = doubleTimeChargeRate,
           Client_Charge_Rate_2_5X__c = doubleTimeAndAHalfChargeRate,
   
           Candidate_Pay_Rate_Flat_Overtime_2XM__c = 0,
           Client_Charge_Rate_Flat_Overtime_2XM__c  = 0
           );
           
        try{
            update vacancy;
        }catch(Exception e){
            System.debug(e);
            return false;
        }
           
        return true;
        
    }
    
    public Boolean saveAllowanceRates(Id vacancyId, List<VacancyRatesService.AllowanceVariable> allowanceVariables){
        
         //assign values to the variables which will be pushed to vacancy object
        String type1 =  allowanceVariables.get(1).name;
        Decimal type1Cost = allowanceVariables.get(1).valueOfCostRate();
        Decimal type1ChargeRate = allowanceVariables.get(1).valueOfChargeRate();

        String type2 = allowanceVariables.get(2).name;
        Decimal type2Cost = allowanceVariables.get(2).valueOfCostRate();
        Decimal type2ChargeRate = allowanceVariables.get(2).valueOfChargeRate();
    
        String type3 =  allowanceVariables.get(3).name;
        Decimal type3Cost = allowanceVariables.get(3).valueOfCostRate();
        Decimal type3ChargeRate = allowanceVariables.get(3).valueOfChargeRate();
    
        String type4 = allowanceVariables.get(4).name;
        Decimal type4Cost = allowanceVariables.get(4).valueOfCostRate();
        Decimal type4ChargeRate = allowanceVariables.get(4).valueOfChargeRate();
        
        String type5 = allowanceVariables.get(5).name;
        Decimal type5Cost = allowanceVariables.get(5).valueOfCostRate();
        Decimal type5ChargeRate = allowanceVariables.get(5).valueOfChargeRate();
        
        /*if((null == type1 || ''.equals(type1))
            && (null == type2 || ''.equals(type2))
            && (null == type3 || ''.equals(type3))
            && (null == type4 || ''.equals(type4))
            && (null == type5 || ''.equals(type5))){
                
            //nothing to do
            return false;
        }*/
        
        if(type1Cost == 0){
        	type1Cost = null;
        }
         if(type2Cost == 0){
        	type2Cost = null;
        }
         if(type3Cost == 0){
        	type3Cost = null;
        }
         if(type4Cost == 0){
        	type4Cost = null;
        }
         if(type5Cost == 0){
        	type5Cost = null;
        }
        
        if(type1ChargeRate == 0){
        	type1ChargeRate = null;
        }
        if(type2ChargeRate == 0){
        	type2ChargeRate = null;
        }
        if(type3ChargeRate == 0){
        	type3ChargeRate = null;
        }
        if(type4ChargeRate == 0){
        	type4ChargeRate = null;
        }
        if(type5ChargeRate == 0){
        	type5ChargeRate = null;
        }
        
        PeopleCloud1__Placement__c vacancy = new PeopleCloud1__Placement__c(
           id = vacancyId,
           Allowance_Type_1_2XM__c = type1,
           Allowance_Type_2_2XM__c = type2,
           Allowance_Type_3_2XM__c = type3,
           Allowance_Type_4_2XM__c = type4,
           Allowance_Type_5_2XM__c = type5,
           Allowance_Cost_1_2XM__c = type1Cost,
           Allowance_Cost_2_2XM__c = type2Cost,
           Allowance_Cost_3_2XM__c = type3Cost,
           Allowance_Cost_4_2XM__c = type4Cost,
           Allowance_Cost_5_2XM__c = type5Cost,
           Allowance_Charge_Rate_1_2XM__c = type1ChargeRate,
           Allowance_Charge_Rate_2_2XM__c = type2ChargeRate,
           Allowance_Charge_Rate_3_2XM__c = type3ChargeRate,
           Allowance_Charge_Rate_4_2XM__c = type4ChargeRate,
           Allowance_Charge_Rate_5_2XM__c = type5ChargeRate
           );
           
        try{
            update vacancy;
        }catch(Exception e){
            System.debug(e);
            return false;
        }
           
        return true;
    }
    
    /**
     * Get the start date of current vacancy contract by id
     */
    public Date getContractStartDate(Id vacancyId){
        
         PeopleCloud1__Placement__c vacancy = [SELECT id,name, PeopleCloud1__Start_Date__c FROM PeopleCloud1__Placement__c WHERE id=:vacancyId];
         if( vacancy.PeopleCloud1__Start_Date__c == null  ){
             return Date.today();

         }else{
             return vacancy.PeopleCloud1__Start_Date__c;
         }
        
    }
    
    /**
     * Get all states from custom setting - State_Rates_Setting__c
     */
    public Set<String> getStates(){
        
        Set<String> stateSet = new Set<String>();
        
        List<State_Rates_Setting__c> stateRatesList = [select State__c from State_Rates_Setting__c  ];
    
        if( stateRatesList != null && stateRatesList.size() > 0  ){
            //remove the duplicated states
            for( State_Rates_Setting__c s : stateRatesList ){
                stateSet.add( s.State__c );
            }
           
        }
        
        return stateSet;
    }
    

    public Decimal getSuperByState( String stateVar, Date effectiveDate){
        State_Rates_Setting__c stateRates = getEffectiveStateRates(stateVar, effectiveDate);
        if( stateRates != null )
            return stateRates.Super__c;
        return 0;
    }
    
    public Decimal getPayrollTaxByState( String stateVar, Date effectiveDate){
        State_Rates_Setting__c stateRates = getEffectiveStateRates(stateVar, effectiveDate);
        if( stateRates != null )
            return stateRates.Payroll_tax__c;
        return 0;
    }
    
    /**
     * Get applied super and payroll tax from custom setting - State_Rates_Setting__c according to the specified state and effective date
     */
    public State_Rates_Setting__c getEffectiveStateRates(String stateVar, Date effectiveDate){
        List<State_Rates_Setting__c> stateRatesList = [select State__c, Super__c,Payroll_tax__c from State_Rates_Setting__c where State__c = :stateVar and Effective_Date__c <= :effectiveDate order by Effective_Date__c desc];
    
        if( stateRatesList != null && stateRatesList.size() > 0  ){
            State_Rates_Setting__c stateRates = stateRatesList[0];
            
            return stateRates;
        }
        
        return null;
    }
    
    private Decimal calculateBaseRate(String rateType){
       
         Decimal result =  stateRateVariables.getBaseRate().valueOf(rateType) ;
         return result.setScale(2, RoundingMode.HALF_UP);
    }
 
    /**
    * Casual Loading $ = Base Rate $ * Casual Loading %
    */
    private Decimal calculateCasualLoading(String rateType){
       
         Decimal result =  stateRateVariables.getBaseRate().valueOf(rateType) 
            * stateRateVariables.getCasualLoading().valueOf(rateType) / 100 ;
         return result.setScale(2, RoundingMode.HALF_UP);
    }
    
    /**
     * Shift Loading $ = Base Rate $ * Shift Loading %
     */
    private Decimal calculateShiftLoading(String rateType){
       
         Decimal result =  stateRateVariables.getBaseRate().valueOf(rateType) 
            * stateRateVariables.getShiftLoading().valueOf(rateType) / 100 ;
         return result.setScale(2, RoundingMode.HALF_UP);
    }
    
    /**
     * Actual Rate $ = Base Rate $ + Casual Loading $ + Shift Loading $
     */
    private Decimal calculateActualRate(String rateType){
        Decimal one = 1.0;
        
        Decimal result = calculateBaseRate(rateType)  + calculateCasualLoading(rateType)  + calculateShiftLoading(rateType);
        if(result == null)
            result = 0.00;
        result = result.setScale(2, RoundingMode.HALF_UP);
       
        return result;
    }
    
    /**
     * Superannuation $ = Actual Rate $ * Superannuation %
     */
    private Decimal calculateSuper(String rateType){
      
         Decimal result =  calculateActualRate(rateType) 
            * stateRateVariables.getSuper().valueOf(rateType) / 100;
         return result.setScale(2, RoundingMode.HALF_UP);
    }
    
    /**
     * Long Service Leave $ = Actual Rate $ * Long Service Leave %
     */
    private Decimal calculateLsl(String rateType){
       
         Decimal result =  calculateActualRate(rateType) 
            * stateRateVariables.getLsl().valueOf(rateType) / 100 ;
         return result.setScale(2, RoundingMode.HALF_UP);
    }
    
    /**
     * Payroll Tax $ = (Actual Rate $ + Superannuation $ + Long Service Leave $) * Payroll Tax %
     */
    private Decimal calculatePayrollTax(String rateType){
         
        Decimal usedSuper = 0.0;
        if(VacancyRatesService.isSuperApplied( rateType ) ){
            usedSuper =  calculateSuper(rateType)  ;
        }
        
         Decimal result = ( calculateActualRate(rateType) + usedSuper  +  calculateLsl(rateType) ) 
            * stateRateVariables.getPayrollTax().valueOf(rateType) / 100;
         return result.setScale(2, RoundingMode.HALF_UP);
    }
    
    /**
     * Workers Comp $ = (Actual Rate $ + Superannuation $ + Long Service Leave $) * Workers Comp %
     */
    private Decimal calculateWorkComp(String rateType){
        
        Decimal usedSuper = 0.0;
        if(VacancyRatesService.isSuperApplied( rateType ) ){
            usedSuper =  calculateSuper(rateType)  ;
        }
         
         Decimal result = ( calculateActualRate(rateType) + usedSuper  +  calculateLsl(rateType) ) 
            * stateRateVariables.getWorkComp().valueOf(rateType) / 100;
         return result.setScale(2, RoundingMode.HALF_UP);
    }
    /**
     * Mark Up $ = (Actual Rate $ + Superannuation $ + Long Service Leave $ + Payroll Tax $ + Workers Comp $) * Mark Up %
     */
    private Decimal calculateMarkUp(String rateType){
        
        Decimal usedSuper = 0.0;
        if(VacancyRatesService.isSuperApplied( rateType ) ){
            usedSuper =  calculateSuper(rateType)  ;
        }
         
         Decimal result = ( calculateActualRate(rateType) 
                + usedSuper 
                + calculateLsl(rateType) 
                + calculatePayrollTax( rateType)
                + calculateWorkComp( rateType)
                ) * stateRateVariables.getMarkUp().valueOf(rateType) / 100;
                
         return result.setScale(2, RoundingMode.HALF_UP);
    }
    
    /**
     * Charge Rate $ = Actual Rate $ + Superannuation $ + Long Service Leave $ + Payroll Tax $ + Workers Comp $ + Mark Up $
     */
    private Decimal calculateChargeRate(String rateType ){
       
        Decimal usedSuper = 0.0;
        if(VacancyRatesService.isSuperApplied( rateType ) ){
            usedSuper =  calculateSuper(rateType)  ;
        }

        Decimal result = calculateActualRate(rateType) 
                        + usedSuper
                        + calculateLsl(rateType) 
                        + calculatePayrollTax( rateType)
                        + calculateWorkComp( rateType)
                        + calculateMarkUp( rateType);
              
        result = result.setScale(2, RoundingMode.HALF_UP);
        
        return result;
        
    }
    
    /**
     * Build a Formula object according to the row title
     */
    public Formula buildFormula( String rowTitle){
        if(BASE_RATE_RATE.equals(rowTitle)){
            return new BaseRateFormula(this);
            
        }else if(CASUAL_LOADING_RATE.equals(rowTitle)){
            return new CasualLoadingFormula(this);
            
        }else if(SHIFT_LOADING_RATE.equals(rowTitle)){
            return new ShiftLoadingFormula(this);
            
        }else if(ACTUAL_RATE.equals(rowTitle)){
            return new ActualRateFormula(this);
            
        }else if(SUPER_RATE.equals(rowTitle)){
            return new SuperFormula(this);
            
        }else if(LSL_RATE.equals(rowTitle)){
            return new LslFormula(this);
            
        }else if(PAYROLL_TAX_RATE.equals(rowTitle)){
            return new PayrollTaxFormula(this);
            
        }else if(WORK_COMP_RATE.equals(rowTitle)){
            return new WorkCompFormula(this);
            
        }else if(MARK_UP_RATE.equals(rowTitle)){
            return new MarkUpFormula(this);
            
        }else if(CHARGE_RATE.equals(rowTitle)){
            return new ChargeRateFormula(this);
            
        }
        
        return null;
    }
    
    /**
     *  Formula interface to be implemented different formulas
     */
    public interface Formula{
        
         Decimal calc( String rateType );
    }
    
    /**
     * 
     */
    // public class AllowanceFormula implements Formula{
     //    
     //    public Decimal calc(String rateType){
     //        
      //   }
    // }
    
    /**
     *  Formula for calculating base rate
     */
    public class BaseRateFormula implements Formula{
        VacancyRatesService service ;
        public BaseRateFormula(VacancyRatesService service){
            this.service = service;
        }
        public Decimal calc( String rateType ){
             return this.service.calculateBaseRate( rateType );
        }
    }

    /**
     *  Formula for calculating CasualLoading rate
     */
    public class CasualLoadingFormula implements Formula{
        VacancyRatesService service ;
        public CasualLoadingFormula(VacancyRatesService service){
            this.service = service;
        }
        public Decimal calc( String rateType ){
             return this.service.calculateCasualLoading( rateType );
        }
    }
    
    /**
     *  Formula for calculating ShiftLoading rate
     */
    public class ShiftLoadingFormula implements Formula{
        VacancyRatesService service ;
        public ShiftLoadingFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculateShiftLoading( rateType );
        }
    }
    
    /**
     *  Formula for calculating ActualRate rate
     */
    public class ActualRateFormula implements Formula{
        VacancyRatesService service ;
        public ActualRateFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculateActualRate( rateType );
        }
    }
    
    /**
     *  Formula for calculating super rate
     */
    public class SuperFormula implements Formula{
        VacancyRatesService service ;
        public SuperFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculateSuper( rateType );
        }
    }
    
    /**
     *  Formula for calculating LSL rate
     */
    public class LslFormula implements Formula{
        VacancyRatesService service ;
        public LslFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculateLsl( rateType );
        }
    }
    
    /**
     *  Formula for calculating Payroll tax rate
     */
    public class PayrollTaxFormula implements Formula{
        VacancyRatesService service ;
        public PayrollTaxFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculatePayrollTax( rateType );
        }
    }
    
    /**
     *  Formula for calculating work comp rate
     */
    public class WorkCompFormula implements Formula{
        VacancyRatesService service ;
        public WorkCompFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculateWorkComp( rateType );
        }
    }
    
    /**
     *  Formula for calculating Mark up rate
     */
    public class MarkUpFormula implements Formula{
        VacancyRatesService service ;
        public MarkUpFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculateMarkUp( rateType );
        }
    }
    
    /**
     *  Formula for calculating Charge rate
     */
    public class ChargeRateFormula implements Formula{
        VacancyRatesService service ;
        public ChargeRateFormula( VacancyRatesService service){
             this.service = service;
        }
        public Decimal calc( String rateType ){
             return  this.service.calculateChargeRate( rateType );
        }
    }
    
    /**
     *  The class defines a object for each row of table 1 and table 2 with columns variables
     */
    public with sharing class RateVariable{
    
        public Integer index{get;set;}
        public String name{get;set;}
        
        public String standardTime{get;set;}
        public String shiftRateOne{get;set;}
        public String shiftRateTwo{get;set;}
        public String timeAndAHalf{get;set;}
        public String doubleTime{get;set;}
        public String doubleTimeAndAHalf{get;set;}
        
        public Boolean disabledOvertime{get;set;}
        
        public String isHighlight{get;set;}
        
        public RateVariable(Integer index, String varname){
            this.index = index;
            this.name = varname;
            isHighlight = 'highlight_' + VacancyRatesService.isHighlightRow(varname);
            disabledOvertime = false;
            standardTime = '0.00';
            shiftRateOne = '0.00';
            shiftRateTwo = '0.00';
            timeAndAHalf = '0.00';
            doubleTime = '0.00';
            doubleTimeAndAHalf = '0.00';
        }  
        
        public RateVariable(
            Integer index, 
            String varname, 
            Decimal standardTime, 
            Decimal shiftRateOne, 
            Decimal shiftRateTwo,  
            Decimal timeAndAHalf,
            Decimal doubleTime,
            Decimal doubleTimeAndAHalf){
            
            this(index, varname);
            this.standardTime = '' + standardTime;
            this.shiftRateOne = '' + shiftRateOne;
            this.shiftRateTwo = '' + shiftRateTwo;
            this.timeAndAHalf = '' + timeAndAHalf;
            this.doubleTime = '' + doubleTime;
            this.doubleTimeAndAHalf = '' + doubleTimeAndAHalf ;
        }
        

         public RateVariable(Integer index, String varname, Boolean disabledOvertime){
             this(index, varname);
             this.disabledOvertime = disabledOvertime;
         }
         
         public RateVariable withColumnValue(String column, Decimal value){
            if(STANDARD_TIME_COL.equals(column)){
               
                standardTime = '' + value;
               
            }else if(SHITF_RATE_ONE_COL.equals(column)){
               
                shiftRateOne = '' + value;
              
            }else if(SHITF_RATE_TWO_COL.equals(column)){
                
                shiftRateTwo = '' + value;
                
            }else if(TIME_AND_AHALF_COL.equals(column)){
                
                timeAndAHalf = '' + value;
               
            }else if(DOUBLE_TIME_COL.equals(column)){
               
                doubleTime = '' + value;
               
            }else if(DOUBLE_TIME_AND_AHALF.equals(column)){
              
                doubleTimeAndAHalf  = '' + value;
               
            }
            
            return this;
        }
        
        public Decimal valueOf(String rateType){
            if(STANDARD_TIME_COL.equals(rateType)){
                if(standardTime == null || standardTime.trim().equals(''))
                    standardTime = '0.00';
                return Decimal.valueOf(standardTime);
            }else if(SHITF_RATE_ONE_COL.equals(rateType)){
                if(shiftRateOne == null || shiftRateOne.trim().equals(''))
                    shiftRateOne = '0.00';
                return Decimal.valueOf(shiftRateOne);
            }else if(SHITF_RATE_TWO_COL.equals(rateType)){
                 if(shiftRateTwo == null || shiftRateTwo.trim().equals(''))
                    shiftRateTwo = '0.00';
                return Decimal.valueOf(shiftRateTwo);
            }else if(TIME_AND_AHALF_COL.equals(rateType)){
                 if(timeAndAHalf == null || timeAndAHalf.trim().equals(''))
                    timeAndAHalf = '0.00';
                return Decimal.valueOf(timeAndAHalf);
                
            }else if(DOUBLE_TIME_COL.equals(rateType)){
                 if(doubleTime == null || doubleTime.trim().equals(''))
                    doubleTime = '0.00';
                return Decimal.valueOf(doubleTime);
            }else if(DOUBLE_TIME_AND_AHALF.equals(rateType)){
                if(doubleTimeAndAHalf == null || doubleTimeAndAHalf.trim().equals(''))
                    doubleTimeAndAHalf = '0.00';
                return Decimal.valueOf(doubleTimeAndAHalf);
            }else{
                return 0.00;
            }
        }
    }
    
    public with sharing class StateRateVariables{
        public String state{get;set;}
        public  List<RateVariable> rateVariables{get;set;}
        public Map<String, RateVariable> variablesTable = new Map<String,RateVariable>();
        
        public RateVariable baseRate;
        public RateVariable casualLoading;
        public RateVariable shiftLoading;
        public RateVariable superan;
        public RateVariable lsl;
        public RateVariable payrollTax;
        public RateVariable workComp;
        public RateVariable markUp;
      
        public List<RateVariable> getRateVariables(){
           return this.rateVariables;
        }
        public StateRateVariables(String state,  List<RateVariable> rateVariables){
            this.state = state;
            this.rateVariables = rateVariables;
            if(rateVariables != null){
                for(RateVariable var :  rateVariables){
                    variablesTable.put(var.name, var);
                }
            }
        }
        
        public RateVariable getBaseRate(){
            if( baseRate == null)
                baseRate = getVariable(BASE_RATE);
            
            return baseRate;
        }
        
        public RateVariable getCasualLoading(){
            if( casualLoading == null){
                
                casualLoading = getVariable(CASUAL_LOADING);
            }
            return casualLoading;
        }
        
        public RateVariable getShiftLoading(){
            if( shiftLoading == null )
                shiftLoading = getVariable(SHIFT_LOADING);
            
            return shiftLoading;
        }
        
         public RateVariable getSuper(){
            if( superan == null )
                superan = getVariable(SUPERA);
            
            return superan;
        }
        
        public RateVariable getLsl(){
            if( lsl  == null )
                return getVariable(LSL_P);
            
            return lsl;
        }
        
         public RateVariable getPayrollTax(){
            if( payrollTax  == null )
                payrollTax = getVariable(PAYROLL_TAX);
            
            return payrollTax;
        }
        
        public RateVariable getWorkComp(){
            if( workComp == null )
                workComp = getVariable(WORK_COMP);
                
            return workComp;
        }
        
        public RateVariable getMarkUp(){
            if(markUp == null )
                markUp = getVariable(MARK_UP);
            
            return markUp;
        }
        
  
        public RateVariable getVariable( String varname ){
            return variablesTable.get(varname);
        }
        
        public RateVariable getVariableOld( String varname ){
            RateVariable var = null;
            for( RateVariable v : this.rateVariables ){
                if(v.name.equals( varname )){
                    var = v;
                   
                    break;
                }
            }
            
            return var;
        }
        
        
    
    }
    
    public with sharing class AllowanceVariable{
    
        public Integer index{get;set;}
        public String name{get;set;}
        public String costRate{get;set;}
        public Decimal totalCostRate{get;set;}
        public String adminFee{get;set;}
        public Decimal chargeRate{get;set;}
        
        public boolean checkedSuper{get;set;}
        public boolean checkedLsl{get;set;}
        public boolean checkedWorkComp{get;set;}
        public boolean checkedPayrollTax{get;set;}
        
        public Decimal appliedSuper{get;set;}
        public Decimal appliedLsl{get;set;}
        public Decimal appliedWorkComp{get;set;}
        public Decimal appliedPayrollTax{get;set;}
        public Boolean applied{get;set;}
       
       
        
        public AllowanceVariable(Integer index, String varname){
            this.index = index;
            this.name = varname;
           
            costRate = '0.00';
            
            totalCostRate = 0.00;
            adminFee = '10.00';
            chargeRate = 0.00;
            
            checkedSuper = true;
            checkedLsl = true;
            checkedWorkComp = true;
            checkedPayrollTax = true;
            
            applied = false;
           
        }  
        
        public AllowanceVariable(
            Integer index, 
            String varname, 
            Decimal costRate, 
            Decimal totalCostRate,  
            Decimal adminFee,
            Decimal chargeRate
           ){
            
            this(index, varname);
            this.costRate = '' + costRate;
            this.totalCostRate = totalCostRate;
            this.adminFee = '' + adminFee;
            this.chargeRate = chargeRate;
            
        }
        
        public Decimal valueOfCostRate(){
             if(costRate == null || costRate.trim().equals(''))
                costRate = '0.00';
            return Decimal.valueOf(costRate);
        }
        
        public Decimal valueOfChargeRate(){
            return chargeRate;
        }

         public AllowanceVariable withTotalCostRate( Decimal value){
             this.totalCostRate = value;
            
            return this;
        }
        
         public AllowanceVariable withChargeRate( Decimal value){
             this.chargeRate = value;
            
            return this;
        }
        
         public AllowanceVariable withAppliedOptions( Boolean applied, Decimal appliedSuper, Decimal appliedLsl, Decimal appliedWorkComp, Decimal appliedPayrollTax){
             this.applied = applied;
             this.appliedSuper = appliedSuper;
             this.appliedLsl = appliedLsl;
             this.appliedWorkComp = appliedWorkComp;
             this.appliedPayrollTax = appliedPayrollTax;
            
            return this;
        }
    }
}