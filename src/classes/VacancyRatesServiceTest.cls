@isTest
public class VacancyRatesServiceTest {
    
    static VacancyRatesService.StateRateVariables stateRateVariables;
    
    static {
	    //init State_Rates_Setting__c
		State_Rates_Setting__c[] ratesList =
					new State_Rates_Setting__c[]{ 
							new State_Rates_Setting__c(
									name='NSW Rates 1',
									State__c='NSW',
									Super__c = 9.5 ,
									Payroll_tax__c=5.45,
									Effective_Date__c= Date.newInstance(2015, 7, 1)),
							new State_Rates_Setting__c(
									name='NSW Rates 2',
									State__c='NSW',
									Super__c = 9.4 ,
									Payroll_tax__c=5.35,
									Effective_Date__c= Date.newInstance(2016, 7, 1)),
							new State_Rates_Setting__c(
									name='NSW Rates 3',
									State__c='NSW',
									Super__c = 9.3 ,
									Payroll_tax__c=5.25,
									Effective_Date__c= Date.newInstance(2014, 7, 1)),
							new State_Rates_Setting__c(
									name='QLD Rates 3',
									State__c='QLD',
									Super__c = 9.8 ,
									Payroll_tax__c=6,
									Effective_Date__c= Date.newInstance(2015, 7, 1))
					};
					
		insert ratesList;
		
		
		 /** 
		  * build rate variables of table 1 for user edit
         */
        List<VacancyRatesService.RateVariable> defaultRateVariables = new  List<VacancyRatesService.RateVariable>();
        defaultRateVariables.add(new VacancyRatesService.RateVariable (1, VacancyRatesService.BASE_RATE, true).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,100));
        defaultRateVariables.add(new VacancyRatesService.RateVariable (2, VacancyRatesService.CASUAL_LOADING).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,20));
        defaultRateVariables.add(new VacancyRatesService.RateVariable (3, VacancyRatesService.SHIFT_LOADING ).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,15));
        defaultRateVariables.add(new VacancyRatesService.RateVariable (4, VacancyRatesService.SUPERA ).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,9.5));
        defaultRateVariables.add(new VacancyRatesService.RateVariable (5, VacancyRatesService.LSL_P).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,1.5));
        defaultRateVariables.add(new VacancyRatesService.RateVariable (6, VacancyRatesService.PAYROLL_TAX).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,5.45));
        defaultRateVariables.add( new VacancyRatesService.RateVariable (7, VacancyRatesService.WORK_COMP).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,8));
        defaultRateVariables.add( new VacancyRatesService.RateVariable (8, VacancyRatesService.MARK_UP).withColumnValue(VacancyRatesService.STANDARD_TIME_COL,15));
        stateRateVariables = new VacancyRatesService.StateRateVariables('NSW', defaultRateVariables);
	
	}
	
	static testMethod void testGetEffectiveStateRates(){
	    
	    VacancyRatesService service = new VacancyRatesService(null);
	    State_Rates_Setting__c rate = service.getEffectiveStateRates('NSW', Date.newInstance(2016, 8 ,15));
	    System.assert(rate != null);
	    System.assertEquals(9.4, rate.Super__c);
	    System.assertEquals(5.35, rate.Payroll_tax__c);
	    
	    rate = service.getEffectiveStateRates('NSW', Date.newInstance(2015, 8 ,15));
	    System.assert(rate != null);
	    System.assertEquals(9.5, rate.Super__c);
	    System.assertEquals(5.45, rate.Payroll_tax__c);
	    
	    rate = service.getEffectiveStateRates('NSW', Date.newInstance(2014, 8 ,15));
	    System.assert(rate != null);
	    System.assertEquals(9.3, rate.Super__c);
	    System.assertEquals(5.25, rate.Payroll_tax__c);
	    
	    rate = service.getEffectiveStateRates('NSW', Date.newInstance(2013, 8 ,15));
	    System.assert(rate == null);
	    
	    rate = service.getEffectiveStateRates('QLD', Date.newInstance(2016, 8 ,15));
	    System.assert(rate != null);
	    System.assertEquals(9.8, rate.Super__c);
	    System.assertEquals(6, rate.Payroll_tax__c);
	    
	    rate = service.getEffectiveStateRates('QLD', Date.newInstance(2014, 8 ,15));
	    System.assert(rate == null);
	    
	}
	
	static testMethod void testGetStates(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     Set<String> states = service.getStates();
	     
	     System.assertEquals(2,states.size());
	     System.assert(states.contains('NSW'));
	     System.assert(states.contains('QLD'));
	    
	}
	 /**
    * Casual Loading $ = Base Rate $ * Casual Loading %
    */
	static testMethod void testCalculateCasualLoading(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(20,service.buildFormula(VacancyRatesService.CASUAL_LOADING_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	/**
     * Shift Loading $ = Base Rate $ * Shift Loading %
     */
	static testMethod void testCalculateShiftLoading(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(15,service.buildFormula(VacancyRatesService.SHIFT_LOADING_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	/**
     * Actual Rate $ = Base Rate $ + Casual Loading $ + Shift Loading $
     */
	static testMethod void testCalculateActualRate(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(135,service.buildFormula(VacancyRatesService.ACTUAL_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	 /**
     * Superannuation $ = Actual Rate $ * Superannuation %
     */
	static testMethod void testCalculateSuper(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(12.83,service.buildFormula(VacancyRatesService.SUPER_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	/**
     * Long Service Leave $ = Actual Rate $ * Long Service Leave %
     */
	static testMethod void testCalculateLsl(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(2.03,service.buildFormula(VacancyRatesService.LSL_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	/**
     * Payroll Tax $ = (Actual Rate $ + Superannuation $ + Long Service Leave $) * Payroll Tax %
     */
	static testMethod void testCalculatePayrollTax(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(8.17,service.buildFormula(VacancyRatesService.PAYROLL_TAX_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	/**
     * Workers Comp $ = (Actual Rate $ + Superannuation $ + Long Service Leave $) * Workers Comp %
     */
	static testMethod void testCalculateWorkComp(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(11.99,service.buildFormula(VacancyRatesService.WORK_COMP_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	
	 /**
     * Mark Up $ = (Actual Rate $ + Superannuation $ + Long Service Leave $ + Payroll Tax $ + Workers Comp $) * Mark Up %
     */
	static testMethod void testCalculateMarkUp(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(25.50,service.buildFormula(VacancyRatesService.MARK_UP_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}
	
	/**
     * Charge Rate $ = Actual Rate $ + Superannuation $ + Long Service Leave $ + Payroll Tax $ + Workers Comp $ + Mark Up $
     */
	static testMethod void testCalculateChargeRate(){
	     VacancyRatesService service = new VacancyRatesService(null);
	     service.setStateRateVariables(stateRateVariables);
	     
	     System.assertEquals(195.52,service.buildFormula(VacancyRatesService.CHARGE_RATE).calc(VacancyRatesService.STANDARD_TIME_COL));
	}

    static testMethod void testGetBaseRate_From_StateRateVariables(){
        System.assertEquals(100,stateRateVariables.getBaseRate().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
    
    static testMethod void testGetCasualLoading_From_StateRateVariables(){
        System.assertEquals(20,stateRateVariables.getCasualLoading().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
    
    static testMethod void testGetShiftLoading_From_StateRateVariables(){
        System.assertEquals(15,stateRateVariables.getShiftLoading().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
    
    static testMethod void testGetSuper_From_StateRateVariables(){
        System.assertEquals(9.5,stateRateVariables.getSuper().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
    
    static testMethod void testGetLsl_From_StateRateVariables(){
        System.assertEquals(1.5,stateRateVariables.getLsl().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
    
    static testMethod void testGetPayrollTax_From_StateRateVariables(){
        System.assertEquals(5.45,stateRateVariables.getPayrollTax().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
    
    static testMethod void testGetWorkComp_From_StateRateVariables(){
        System.assertEquals(8,stateRateVariables.getWorkComp().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
    
     static testMethod void testGetMarkUp_From_StateRateVariables(){
        System.assertEquals(15,stateRateVariables.getMarkUp().valueOf(VacancyRatesService.STANDARD_TIME_COL));
    }
}